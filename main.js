// Responsive Navigation Bar
function myFunction() {
  // createing a var and initialising it 
    var x = document.getElementById("myTopnav");
    // making a if else loop
    if (x.className === "topnav") {
      x.className += " responsive";
    } else {
      x.className = "topnav";
    }
  }
  
//   Responsive Side NavBar
// making a function
function openNav() {
    document.getElementById("mySidenav").style.width = "250px";
  }
  
  function closeNav() {
    document.getElementById("mySidenav").style.width = "0";
  }

// bottom to top javascript starts here
//Get the button
var mybutton = document.getElementById("myBtn");

// When the user scrolls down 20px from the top of the document, show the button
window.onscroll = function() {scrollFunction()};

function scrollFunction() {
  if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
    mybutton.style.display = "block";
  } else {
    mybutton.style.display = "none";
  }
}

// When the user clicks on the button, scroll to the top of the document
function topFunction() {
  document.body.scrollTop = 0;
  document.documentElement.scrollTop = 0;
}


  // slideshow javascript starts from here
  let slideIndex = 1;
showSlides(slideIndex);

function plusSlides(n) {
  showSlides(slideIndex += n);
}

function currentSlide(n) {
  showSlides(slideIndex = n);
}

function showSlides(n) {
  let i;
  var slides = document.getElementsByClassName("mySlides");
  var info = document.getElementsByClassName("info");
  let dots = document.getElementsByClassName("dot");
  if (n > slides.length) {slideIndex = 1}    
  if (n < 1) {slideIndex = slides.length}
  for (i = 0; i < slides.length; i++) {
    slides[i].style.display = "none"; 
    info[i].style.display = "none"; 
  }
  for (i = 0; i < dots.length; i++) {
    dots[i].className = dots[i].className.replace(" active", "");
  }
  slides[slideIndex-1].style.display = "block";
  info[slideIndex-1].style.display = "block";  
  dots[slideIndex-1].className += " active";
}

// slideshow paragragh info
var slideshowInfo = document.getElementsByClassName('slideshow-info');
let duckImg = document.getElementById("duck_img");
let potteryImg = document.getElementById("pottery_img");
let watchImg = document.getElementById("watch_img");
let duck = document.getElementById("duck");
let pottery = document.getElementById("pottery");
let watch = document.getElementById("watch");

function info() {
  if (duckImg.style.display = "block") {
    duck.style.display = "block";
  } else if (potteryImg.style.display = "block"){
    pottery.style.display = "block";
    duck.style.display = "none";
  }
  else {
    watch.style.display = "block";
    duck.style.display = "none";
  }
}
